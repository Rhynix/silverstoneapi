<?php

namespace App\Models\Tyres;

use Illuminate\Database\Eloquent\Model;

class Tyres extends Model
{
    public $fillable = [
        'number','description','size','linkname','image','sectionalwidth','aspectratio','brand','type','rimdiameter','designation','liss','productgroupcode','inventory','unitprice','itemcategory','searchdescription','itemsubcategory'
    ];
}
