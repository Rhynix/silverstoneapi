<?php

namespace App\Http\Controllers\Tyres;

use App\Http\Controllers\Controller;
use App\Models\Tyres\Tyres;
use Illuminate\Http\Request;

class TyresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $savedata = Tyres::create([
            'number'        => $request->number,
            'description'   => $request->description,
            'size'          => $request->size,
            'linkname'      => $request->linkname,
            'image'         => $request->image,
            'sectionalwidth'=> $request->sectionalwidth,
            'aspectratio'   => $request->aspectratio,
            'brand'         => $request->brand,
            'type'          => $request->type,
            'rimdiameter'   => $request->rimdiameter,
            'designation'   => $request->designation,
            'liss'          => $request->liss,
            'productgroupcode'=> $request->productgroupcode,
            'inventory'      => $request->inventory,
            'itemcategory'  => $request->itemcategory,
            'searchdescription'=> $request->searchdescription,
            'itemsubcategory'=> $request->itemsubcategory
        ]);
        return response()->json(['data' => $savedata ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
