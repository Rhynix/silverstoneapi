<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeypointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keypoint', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image', 200);
            $table->string('title', 300);
            $table->text('para');
            $table->bigInteger('parent_id')->unsigned()->index()->nullable();
            $table->bigInteger('brand_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('parent_id')->references('id')->on('keypoint');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keypoint');
    }
}
