<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTyresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tyres', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('number', 50);
            $table->string('description', 255);
            $table->string('size', 30);
            $table->string('linkname', 50);
            $table->string('image', 50);
            $table->integer('sectionalwidth');
            $table->string('aspectratio', 50);
            $table->bigInteger('brand')->unsigned()->index();
            $table->string('type', 50)->nullable();
            $table->integer('rimdiameter');
            $table->string('designation', 50)->nullable();
            $table->string('liss', 50)->nullable();
            $table->string('productgroupcode', 50)->nullable();
            $table->string('inventory', 50)->nullable();
            $table->double('unitprice')->nullable();
            $table->string('itemcategory', 50)->nullable();
            $table->string('searchdescription', 200)->nullable();
            $table->string('itemsubcategory', 50)->nullable();
            $table->timestamps();

            $table->foreign('brand')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tyres');
    }
}
