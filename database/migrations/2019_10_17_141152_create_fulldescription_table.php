<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFulldescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fulldescription', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 300)->nullable();
            $table->string('subtitle', 400)->nullable();
            $table->bigInteger('brand_id')->unsigned()->index();
            $table->text('para');
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fulldescription');
    }
}
