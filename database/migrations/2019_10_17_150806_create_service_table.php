<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 300)->nullable();
            $table->string('image', 200)->nullable();
            $table->text('para');
            $table->bigInteger('parent_id')->unsigned()->index()->nullable();
            $table->bigInteger('child_id')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('service');
            $table->foreign('child_id')->references('id')->on('service');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdervice');
    }
}
